FROM alpine:latest
RUN mkdir /learn-grpc

WORKDIR /learn-grpc

ADD careplan-service /learn-grpc/careplan-service

CMD ["./careplan-service"]
