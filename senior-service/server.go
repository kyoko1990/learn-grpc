package main

import (
	pb "learn-grpc/senior-service/proto/senior"
	careplanpb "learn-grpc/careplan-service/proto/careplan"
	"context"
	"net"
	"log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	PORT = ":10000"
	ADDRESS = "localhost:10001"
)

type IDomain interface {
	Create(senior *pb.Senior) (*pb.Senior,error)
	GetAll() []*pb.Senior
}

type Domain struct {
	seniors []*pb.Senior
}

func (domain *Domain) Create(senior *pb.Senior) (*pb.Senior,error) {
	domain.seniors = append(domain.seniors,senior)
	return senior,nil
}

func (domain *Domain) GetAll() []*pb.Senior {
	return domain.seniors
}

type service struct {
	domain Domain
	careplanClient careplanpb.CareplanServiceClient
}

func (s *service) CreateSenior(ctx context.Context,req *pb.Senior) (*pb.Response,error) {

	careplanResponse,err := s.careplanClient.CareplanGenerate(context.Background(),&careplanpb.Senior{Age: req.Age,})
	log.Printf("Found careplan : %s \n",careplanResponse.Careplan.Name)
	if err != nil {
		return nil,err
	}
	senior,err := s.domain.Create(req)
	if err != nil {
		return nil,err
	}
	resp := &pb.Response{Created:true,Senior: senior}
	return resp,nil
}

func (s *service) GetSeniors(ctx context.Context,req *pb.GetRequest) (*pb.Response,error) {
	allSeniors := s.domain.GetAll()
	resp :=&pb.Response{Seniors:allSeniors}
	return resp,nil
}

func main() {
	lis,err := net.Listen("tcp",PORT)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Printf("listen on :%s\n",PORT)

	server := grpc.NewServer()
	conn,err := grpc.Dial(ADDRESS,grpc.WithInsecure())
	if err != nil {
		log.Fatalf("connect error : %v",err)
	}
	defer conn.Close()
	careplanClient := careplanpb.NewCareplanServiceClient(conn)

	d := Domain{}
	pb.RegisterSeniorServiceServer(server,&service{d,careplanClient})

	reflection.Register(server)
	if err := server.Serve(lis); err != nil {
		log.Fatalf("failed to serve :%v",err)
	}

}
