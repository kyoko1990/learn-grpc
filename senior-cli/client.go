package main

import (
	pb "learn-grpc/senior-service/proto/senior"
	"io/ioutil"
	"encoding/json"
	"google.golang.org/grpc"
	"log"
	"context"
)

const (
	ADDRESS="localhost:10000"
	DEFAULT_INFO_FILE = "senior.json"
)

func parseFile(fileName string) (*pb.Senior,error) {
	data,err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil,err
	}
	var senior *pb.Senior
	json.Unmarshal(data,&senior)
	return senior,nil
}


func main() {
	conn,err := grpc.Dial(ADDRESS,grpc.WithInsecure())
	if err!= nil{
		log.Fatalf("connect error:%v",err)
	}

	defer conn.Close()

	client :=pb.NewSeniorServiceClient(conn)

	infoFile := DEFAULT_INFO_FILE
	s,err := parseFile(infoFile)
	if err != nil {
		log.Fatalf("parseFile fail")
	}

	resp ,err := client.CreateSenior(context.Background(),s)
	log.Printf("created:%t",resp.Created)

	getAll, err := client.GetSeniors(context.Background(),&pb.GetRequest{})
	if err != nil {
		log.Fatalf("Could not list senirs:%v", err)
	}
	for _,v := range getAll.Seniors {
		log.Println(v)
	}
}

