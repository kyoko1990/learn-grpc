package main

import (
	pb "learn-grpc/careplan-service/proto/careplan"
	"context"
	"google.golang.org/grpc"
	"log"
	"net"
)

const (
	port = ":10001"
)

type IRepository interface {
	CareplanGenerate(*pb.Senior) (*pb.Careplan,error)
}

type CareplanRepository struct {
	careplans []*pb.Careplan
}

func (repo *CareplanRepository) CareplanGenerate(senior *pb.Senior) (*pb.Careplan,error) {
	for _, c := range repo.careplans {
		if c.MaxAge > senior.Age {
			return c,nil
		}
	}
	return nil,nil
}

type service struct {
	repo CareplanRepository
}

func (s *service) CareplanGenerate(ctx context.Context, senior *pb.Senior) (*pb.Response,error){
	c, err := s.repo.CareplanGenerate(senior)
	resp := &pb.Response{Suitable:true,Careplan:c}
	return resp,err
}

func main() {
	lis,err := net.Listen("tcp",port)
	if err != nil {
		log.Fatalf("failed to listen : %v",err)
	}
	log.Printf("listen on :%s ",port)
	careplans := []*pb.Careplan{
		{Id : "careplan1",Name:"Bathing",MaxAge:100},
	}
	repo := CareplanRepository{careplans}
	server := grpc.NewServer()

	pb.RegisterCareplanServiceServer(server, &service{repo})

	if err := server.Serve(lis); err != nil {
		log.Fatalf("failed to serve : %v",err)
	}
}
