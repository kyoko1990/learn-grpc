from concurrent import futures

import time
import logging

import grpc

from proto.careplan import careplan_pb2
from proto.careplan import careplan_pb2_grpc
_ONE_DAY_IN_SECONDS = 60*60*24

class CareplanServiceServicer(careplan_pb2_grpc.CareplanServiceServicer):

    def __init__(self):
        self.careplans = [{'id':'1','name':'careplan3','max_age':100 }]

    def CareplanGenerate(self,request,context):
        for careplan in self.careplans:
            if careplan['max_age'] > request.age:
                return careplan_pb2.Response(careplan=careplan,suitable = True)
        return None

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers = 10))
    careplanservice = CareplanServiceServicer()

    careplan_pb2_grpc.add_CareplanServiceServicer_to_server(careplanservice,server)

    server.add_insecure_port('[::]:10001')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    logging.basicConfig()
    serve()


